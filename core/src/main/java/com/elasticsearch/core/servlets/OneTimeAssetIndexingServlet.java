package com.elasticsearch.core.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.servlet.Servlet;
import javax.servlet.ServletException;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.elasticsearch.action.index.IndexResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.day.cq.search.PredicateGroup;
import com.day.cq.search.Query;
import com.day.cq.search.QueryBuilder;
import com.day.cq.search.result.Hit;
import com.day.cq.search.result.SearchResult;
import com.elasticsearch.core.service.ElasticSearchService;

@Component(service = Servlet.class, name = "OneTimeJob Servlet", property = { "sling.servlet.paths=/bin/onetimejob",
		"sling.servlet.methods=GET/POST" })
public class OneTimeAssetIndexingServlet extends SlingAllMethodsServlet {
	private static final long serialVersionUID = 1L;

	@Reference
	ElasticSearchService els; // Invoking Custom Service of IndexRequestService

	@Override
	protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response)
			throws ServletException, IOException {

		// Creating Object for PrintWriter
		PrintWriter writer = response.getWriter();

		// Creating QueryBuilder Object to get required nodes.
		QueryBuilder qb = request.getResourceResolver().adaptTo(QueryBuilder.class);
		Map<String, String> qmap = new HashMap<String, String>();
		qmap.put("path", "/content/dam/wknd-events");
		qmap.put("property", "jcr:primaryType");
		qmap.put("property.value", "dam:Asset");
		qmap.put("p.limit", "-1");
		Query query = qb.createQuery(PredicateGroup.create(qmap),
				(Session) request.getResourceResolver().adaptTo(Session.class));
		SearchResult result = query.getResult();
		if (result != null) {
			List<Hit> hits = result.getHits();
			response.getWriter().write("\nHits: " + hits.size());
			int i = 1;
			for (Hit hit : hits) {
				try {
					writer.write("\n******  " + i + " Title: " + hit.getTitle() + "  ******\n");

					// Calling Service to get IndexResponse
					IndexResponse indexResponse = els.indexAsset(hit.getPath());

					writer.println(indexResponse);
					i++;
				} catch (RepositoryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

}
