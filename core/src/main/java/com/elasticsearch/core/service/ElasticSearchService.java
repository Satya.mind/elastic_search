package com.elasticsearch.core.service;

import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.index.reindex.BulkByScrollResponse;

public interface ElasticSearchService {
	
//	RestHighLevelClient getRestHighLevelClient();
	
//	List<String> getElasticSearchIndexFields();
	
	IndexResponse indexAsset(String path);
	
	//TBD
	BulkByScrollResponse updateAssetIndex(String path);
	
	BulkByScrollResponse deleteAssetIndex(String path);
	
	//Search method to be declared here and implemented in impl
	
	
}
