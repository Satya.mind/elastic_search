package com.elasticsearch.core.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpHost;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ModifiableValueMap;
import org.apache.sling.api.resource.PersistenceException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.ValueMap;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.Designate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.day.cq.dam.api.Asset;

@Component(service = ElasticSearchService.class, name = "Elastic Search Service")
@Designate(ocd = ElasticSearchConfig.class)
public class ElasticSearchServiceImpl implements ElasticSearchService {

	Logger loger = LoggerFactory.getLogger(this.getClass());

	private ElasticSearchConfig config;

	@Reference
	ResourceResolverFactory factory;

	@Activate
	private void activeConfig(ElasticSearchConfig config) {
		this.config = config;
	}

	@Override
	public IndexResponse indexAsset(String path) {
		// Invoke configured ResourceResolver
		ResourceResolver resolver = getConfiguredResourceResolver();

		// Invoke RestHightLevelClient from Configuration
		RestHighLevelClient client = getRestHighLevelClient();

		// Invoke Index fields from Configuration
		List<String> fields = getElasticSearchIndexFields();
		loger.info("Index Fieds from Configuration: " + fields.toString());

		Resource resource = resolver.getResource(path);

		// Create Map to set index data
		Map<String, Object> data = new HashMap<String, Object>();

		String absPath = resource.getPath();
		String name = resource.getName();

		data.put("title", name);
		data.put("absolutepath", absPath);

		ValueMap valueMap = resource.getValueMap();
		Set<String> keySet = valueMap.keySet();

		for (String key : keySet) {
			if (key.equalsIgnoreCase("jcr:created")) {
				Date date = valueMap.get(key, Date.class);
				SimpleDateFormat formatdate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
				String finaldate = formatdate.format(date);
				data.put(key, finaldate);// createdDate
			} else if (fields.contains(key)) {
				data.put(key, valueMap.get(key));
			}
		}

		Asset asset = resource.adaptTo(Asset.class);
		Map<String, Object> metadata = asset.getMetadata();
		Set<String> set = metadata.keySet();
		loger.info("Asset API Called");

		for (String key : set) {
			if (fields.contains(key)) {
				data.put(key, metadata.get(key));
			}
		}

		// Create Index Request & pass index data as source
		IndexRequest indexRequest = new IndexRequest("dam_asset").source(data);

		IndexResponse response = null;
		try {
			response = client.index(indexRequest, RequestOptions.DEFAULT);
		} catch (IOException e) {
			loger.debug(e.toString());
		}

		if (response != null) {
			String ind = response.getIndex();
			String id = response.getId();
			loger.info("\nIndex: " + ind + "\nId: " + id);

			// Adding both index & id as properties to 'metadata'
			addIndexToMetadata(path, ind, id);
		}
		return response;
	}

	@Override
	public BulkByScrollResponse updateAssetIndex(String path) {
		// Invoke RestHightLevelClient from Configuration
		RestHighLevelClient client = getRestHighLevelClient();

		// Create DeleteByQueryRequest & set query to match 'absolutepath'
		DeleteByQueryRequest deleterequest = new DeleteByQueryRequest("dam_asset");
		deleterequest.setConflicts("proceed");
		deleterequest.setQuery(new TermQueryBuilder("absolutepath.keyword", path));

		BulkByScrollResponse bulkresponse = null;
		try {
			// Invoking BulkByScrollResponse by using RestHighLevelClient &
			// DeleteByQueryRequest
			bulkresponse = client.deleteByQuery(deleterequest, RequestOptions.DEFAULT);

			loger.info("\nBulk Response: " + bulkresponse.toString());

		} catch (IOException e) {
			loger.debug(e.toString());
		}
		return bulkresponse;
	}

	@Override
	public BulkByScrollResponse deleteAssetIndex(String path) {
		// TODO Auto-generated method stub
		return null;
	}

	private RestHighLevelClient getRestHighLevelClient() {
		RestHighLevelClient client = new RestHighLevelClient(
				RestClient.builder(new HttpHost(config.getESHost(), config.getESPort())));
		loger.info(client.toString());
		return client;
	}

	private ResourceResolver getConfiguredResourceResolver() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put(ResourceResolverFactory.SUBSERVICE, "ELASTICKIBANA");

		ResourceResolver resolver = null;
		try {
			resolver = factory.getServiceResourceResolver(map);
		} catch (LoginException e) {
			loger.debug(e.toString());
		}
		return resolver;
	}

	private List<String> getElasticSearchIndexFields() {
		List<String> fields = Arrays.asList(config.getEsFields());
		return fields;
	}

	private void addIndexToMetadata(String parentPath, String elasticIndex, String elasticId) {

		// Get Configured ResourceResolver
		ResourceResolver resolver = getConfiguredResourceResolver();

		// Get the Resource of 'metadata'
		Resource resource = resolver.getResource(parentPath.concat("/jcr:content/metadata"));

		try {
			// Create ModifiableValueMap to add index & id as properties
			ModifiableValueMap valueMap = resource.adaptTo(ModifiableValueMap.class);
			valueMap.put("elasticIndex", elasticIndex);
			valueMap.put("elasticId", elasticId);
			resource.getResourceResolver().commit();

		} catch (PersistenceException e) {
			loger.debug(e.toString());
		}
	}

}
