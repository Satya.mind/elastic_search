package com.elasticsearch.core.listeners;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.apache.sling.api.resource.observation.ResourceChange;
import org.apache.sling.api.resource.observation.ResourceChangeListener;
import org.elasticsearch.client.RestHighLevelClient;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//TBD

@Component(service = ResourceChangeListener.class, property = { ResourceChangeListener.PATHS + "=" + "/content/dam/",
		ResourceChangeListener.CHANGES + "=" + "CHANGED" })
public class UpdateAssetEventListener implements ResourceChangeListener {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	@Reference
	ResourceResolverFactory resolverFactory;

//	@Reference
//	ConfigurationService cs;

	private ResourceResolver resourceResolver;

	@Override

	public void onChange(List<ResourceChange> resourceChanges) {
		log.info("Changed Resources " + resourceChanges.toString());
		String value = null;
		// initialize resource resolver
		Map<String, Object> param = new HashMap<String, Object>();
		param.put(ResourceResolverFactory.SUBSERVICE, "ELASTICKIBANA");

		try {
			resourceResolver = resolverFactory.getServiceResourceResolver(param);
		} catch (LoginException e1) {
			log.error("Unable to fetch resource resolver");
			return;
		}

		if (resourceResolver == null) {
			log.error("Null resource resolver check access to content/dam");
			return;
		}
		RestHighLevelClient client = null;
//		client = cs.getRestHighLevelClient();

		Iterator<ResourceChange> itr = resourceChanges.iterator();
//		while (itr.hasNext()) {
//			String path = itr.next().getPath();
//			log.info(path);
//			try {
//				Resource resource = resourceResolver.getResource(path);
//				String finalPath = resource.getParent().getPath();
//				// String name=resource.getName();
//				ValueMap values = resource.getValueMap();
//				Set<String> keySet = values.keySet();
//				for (String key : keySet) {
//					if (key.equalsIgnoreCase("jcr:title")) {
//						value = String.valueOf(values.get(key));
//						log.info(value);
//
//					}
//				}
//				UpdateByQueryRequest request = new UpdateByQueryRequest("dam_index");
//				request.setConflicts("proceed");
//				Map<String, Object> parameters = new HashMap<>();
//				parameters.put("title", value);
//				Script inline = new Script(ScriptType.INLINE, "painless", "ctx._source.title =params.title",
//						parameters);
//				request.setQuery(new TermQueryBuilder("absolutePath", finalPath)).setScript(inline);
//				BulkByScrollResponse bulkResponse = client.updateByQuery(request, RequestOptions.DEFAULT);
//				List<SearchFailure> failures = bulkResponse.getSearchFailures();
//				Iterator<SearchFailure> failitr = failures.iterator();
//				while (failitr.hasNext()) {
//					SearchFailure eachFail = failitr.next();
//					String index = eachFail.getIndex();
//					log.info(index);
//				}
//
//			} catch (ElasticsearchException | IOException exception) {
//
//			}
//		}

	}

}
