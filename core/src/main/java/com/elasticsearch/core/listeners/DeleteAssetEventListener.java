package com.elasticsearch.core.listeners;

import java.util.Iterator;
import java.util.List;

import org.apache.sling.api.resource.observation.ResourceChange;
import org.apache.sling.api.resource.observation.ResourceChangeListener;
import org.elasticsearch.index.reindex.BulkByScrollResponse;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.elasticsearch.core.service.ElasticSearchService;

/*
 * This Class activates the EvenTListener with respect to Delete Event on given EventPath.
 * 
 * */

@Component(service = ResourceChangeListener.class, property = { ResourceChangeListener.PATHS + "=" + "/content/dam/",
		ResourceChangeListener.CHANGES + "=" + "REMOVED", }, name = "Delete Event Listener")
public class DeleteAssetEventListener implements ResourceChangeListener {

	private Logger loger = LoggerFactory.getLogger(this.getClass());

	@Reference
	ElasticSearchService els;

	@Override
	public void onChange(List<ResourceChange> changes) {

		Iterator<ResourceChange> listIterator = changes.iterator();
		while (listIterator.hasNext()) {
			ResourceChange resourceChange = (ResourceChange) listIterator.next();
			String path = resourceChange.getPath();
			if (!(path.contains("jcr:content"))) {

				loger.info("Final Filtered Path: " + path);

				BulkByScrollResponse deleteResponse = els.deleteAssetIndex(path);

				loger.info("Deleted Indexes: " + deleteResponse);
			}
		}
	}
}